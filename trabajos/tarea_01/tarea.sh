#!/bin/bash

REG=/tmp/script.txt
ERROR=/tmp/errores.txt

echo "Bienvenido usuario: $USER" | tee -a $REG
echo ""
echo "Estos son los procesos y sus subprocesos actuales: 
$(pstree)" | tee -a $REG
echo ""
echo "Estos son los archivos del directorio actual: 
$(ls $pwd)" | tee -a $REG
echo""
echo "Estos son los archivos en el /home del administrador: $(ls /home/administrador 2> $ERROR)" | tee -a $REG
echo ""

echo "El script se copia en '$REG' y si hay errores en '$ERROR'"
echo "Tu terminal: $(tty)" | tee -a $REG
echo "Este programa se ejecuto el: $(date)" | tee -a $REG
